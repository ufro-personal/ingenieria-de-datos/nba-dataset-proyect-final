# Ingeniera de datos

Proyecto Semestral realizado durante el primer semestre del año 2022, para el curso Ingenieria de Datos de la carrera Ingenieria Informatica en la Universidad de La Frontera.

## Integrantes

**Javier Burgos**

**Diego Pincheira**

**Jorge Delgado**

## Analisis Dataset NBA

El conjunto de datos consta de 5 datasets de los cuales se ultilizaran los dataset *games_details.csv* y *games.csv*.

## Estructura

**a. Introducción: plantear el problema y la motivación.**

**b. Exploración de datos.**

**c. Preguntas y problemas**

**d. Propuesta experimental.**


## Repositorios del proyecto de asignatura
- [ ] [Hito 1] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect/-/blob/main/hito1.html
- [ ] [Hito 2] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/main/hito_2.html
- [ ] [Hito 3] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/main/Hito3_G7_html.html

## Reportes y presentaciones del proyecto de asignatura

- [ ] [Hito 1] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect/-/blob/main/PRESENTACION.pdf
- [ ] [Hito 2] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/feature/diego/reporte_hito_2.pdf
- [ ] [Hito 3 REPORTE] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/develop/hito3_G7_reporte.pdf
- [ ] [Hito 3 PRESENTACION] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/develop/Hito3_G7_presentacion.pdf
- [ ] [Hito 3 VIDEO] https://gitlab.com/ufro-personal/ingenieria-de-datos/nba-dataset-proyect-final/-/blob/develop/Hito3_G7_video.mkv

## Herramientas
 
Desarrollo en RStudio para entrega HITO 1, con posterior modificacion, para HITO 2 e HITO 3, en lenguaje Python con Jupyter Notebook y/o Google Colab, mediante el uso de pandas y la libreria sklearn.
